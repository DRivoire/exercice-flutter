import 'package:flutter/material.dart';

void main() {
  final barColor = const Color(0xFF1287A5);
  final bgColor = const Color(0xFFDAE0E2);
  
  var app = MaterialApp(
    home: Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: barColor,

        child: Icon(
          Icons.build),
        onPressed: () {}),
      backgroundColor: bgColor,
      appBar: AppBar(
        title: Text('Instagram'),
        backgroundColor: barColor,
      ),
    ),
  );

  runApp(app);
}